package com.cc221021.wastenotwizard.ui.view_model

import android.util.Log
import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.cc221021.wastenotwizard.data.RecipeDao
import com.cc221021.wastenotwizard.data.model.Recipe
import com.cc221021.wastenotwizard.ui.view.Screen
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class MainViewModel(private val dao: RecipeDao) : ViewModel() {
    private val _mainViewState = MutableStateFlow(MainViewState())
    val mainViewState: StateFlow<MainViewState> = _mainViewState.asStateFlow()

    private val _searchedRecipes = MutableStateFlow<List<Recipe>>(emptyList())
    val searchedRecipes: StateFlow<List<Recipe>> = _searchedRecipes.asStateFlow()

    private val _selectedIngredients = mutableStateOf<List<String>>(emptyList())
    val selectedIngredients: State<List<String>> = _selectedIngredients

    private val _totalRecipeCount = MutableLiveData<Int>()
    val totalRecipeCount: LiveData<Int>
        get() = _totalRecipeCount

    private val _diets = MutableStateFlow<List<String>>(emptyList())
    val diets: StateFlow<List<String>> = _diets.asStateFlow()

    private val _cookingTimes = MutableStateFlow<List<String>>(emptyList())
    val cookingTimes: StateFlow<List<String>> = _cookingTimes.asStateFlow()

    private val _selectedDiet = mutableStateOf<String?>(null)
    val selectedDiet: State<String?> = _selectedDiet

    fun updateUserProfileDiet(diet: String) {
        _selectedDiet.value = diet
    }

    init {
        refreshTotalRecipeCount()
        viewModelScope.launch {
            fetchDietsAndCookingTimes()
        }
    }

    fun selectScreen(screen: Screen) {
        _mainViewState.update { it.copy(selectedScreen = screen) }
    }

    fun saveButton(name: String, selectedIngredients: String, diet: String, time: String, notes: String) {
        viewModelScope.launch {
            val recipe = Recipe(name, selectedIngredients, diet, time, notes)
            dao.insertRecipe(recipe)
            refreshTotalRecipeCount()
            _mainViewState.value = _mainViewState.value.copy(openDialog = true)
        }
    }

    fun getRecipes() {
        viewModelScope.launch {
            dao.getRecipes().collect { recipes ->
                _mainViewState.update { it.copy(recipes = recipes) }
            }
        }
    }

    fun clickDelete(recipe: Recipe) {
        viewModelScope.launch {
            dao.deleteRecipe(recipe)
            getRecipes()
            refreshTotalRecipeCount()
        }
    }

    fun editRecipe(recipe: Recipe) {
        _mainViewState.update { it.copy(openDialog = true, editRecipe = recipe) }
    }

    fun saveEditedRecipe(name: String, selectedIngredients: String, diet: String, time: String, notes: String, recipeId: Int) {
        dismissDialog()
        viewModelScope.launch {
            val recipe = Recipe(name, selectedIngredients, diet, time, notes, recipeId)
            dao.updateRecipe(recipe)
            getRecipes()
        }
    }

    fun dismissDialog() {
        _mainViewState.update { it.copy(openDialog = false) }
    }

    fun updateSelectedIngredients(ingredients: List<String>) {
        _selectedIngredients.value = ingredients
    }

    fun searchRecipesWithIngredients() {
        viewModelScope.launch {
            val selectedIngredients = _selectedIngredients.value.map { it.lowercase() }

            val formattedIngredients = selectedIngredients.map { "%$it%" }

            val paddedIngredients = List(3) { if (it < formattedIngredients.size) formattedIngredients[it] else "" }

            val recipes = dao.searchRecipeByIngredients(paddedIngredients[0], paddedIngredients[1], paddedIngredients[2])

            _searchedRecipes.value = recipes
            Log.d("SEARCH_SCREEN", "Search Recipes: $recipes")
        }
    }

    private suspend fun fetchDietsAndCookingTimes() {
        val diets = dao.getDiets()
        val cookingTimes = dao.getCookingTimes()

        _diets.value = diets
        _cookingTimes.value = cookingTimes
    }

    private fun refreshTotalRecipeCount() {
        viewModelScope.launch {
            _totalRecipeCount.value = fetchTotalRecipeCount()
        }
    }

    private suspend fun fetchTotalRecipeCount(): Int {
        return withContext(Dispatchers.IO) {
            dao.getTotalRecipeCount()
        }
    }
}
