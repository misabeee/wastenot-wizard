@file:OptIn(ExperimentalMaterial3Api::class)

package com.cc221021.wastenotwizard.ui.view

import android.util.Log
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.icons.Icons
import androidx.compose.material3.AlertDialog
import androidx.compose.material3.Button
import androidx.compose.material3.DropdownMenu
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.cc221021.wastenotwizard.ui.view_model.MainViewModel
import androidx.compose.material.DropdownMenuItem
import androidx.compose.material3.MaterialTheme
import com.cc221021.wastenotwizard.ui.theme.Typography
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.material.TextFieldDefaults
import androidx.compose.material.icons.filled.ArrowDropDown
import androidx.compose.material.icons.filled.Edit
import androidx.compose.material3.*
import androidx.compose.material3.ButtonDefaults
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.shadow
import androidx.compose.material3.Checkbox as Checkbox
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.icons.filled.Add
import androidx.compose.material.icons.filled.Delete
import androidx.compose.material.icons.filled.List
import androidx.compose.material.icons.filled.Person
import androidx.compose.material.icons.filled.Search
import androidx.compose.ui.res.painterResource
import com.cc221021.wastenotwizard.R
import androidx.compose.material3.NavigationBar
import androidx.compose.material3.NavigationBarItem
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.withStyle

sealed class Screen(val route: String){
    object Zero: Screen("zero")
    object One: Screen("one")
    object Two: Screen("two")
    object Three: Screen("three")
    object Four: Screen("four")
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun MainView(mainViewModel: MainViewModel){
    val state = mainViewModel.mainViewState.collectAsState()
    val navController = rememberNavController()

    var navigateToZero by remember { mutableStateOf(true) }

    Scaffold(
        bottomBar = { BottomNavigationBar(navController, state.value.selectedScreen) }
    ) {
        NavHost(
            navController = navController,
            modifier = Modifier.padding(it),
            startDestination = if (navigateToZero) Screen.Zero.route else Screen.One.route
        ) {
            composable(Screen.Zero.route) {
                mainViewModel.selectScreen(Screen.Zero)
                startScreen(mainViewModel) {
                    navigateToZero = false
                    navController.navigate(Screen.One.route)
                }
            }
            composable(Screen.One.route) {
                mainViewModel.selectScreen(Screen.One)
                mainScreen(mainViewModel)
            }
            composable(Screen.Two.route) {
                mainViewModel.selectScreen(Screen.Two)
                mainViewModel.getRecipes()
                recipeScreen(mainViewModel)
            }
            composable(Screen.Three.route) {
                mainViewModel.selectScreen(Screen.Three)
                searchScreen(mainViewModel)
            }
            composable(Screen.Four.route) {
                mainViewModel.selectScreen(Screen.Four)
                userScreen(mainViewModel)
            }
        }
    }
}

@Composable
fun BottomNavigationBar(navController: NavHostController, selectedScreen: Screen) {
    if (selectedScreen != Screen.Zero) {
        NavigationBar(
            modifier = Modifier,
            containerColor = MaterialTheme.colorScheme.primary,
        ) {
            NavigationBarItem(
                selected = (selectedScreen == Screen.One),
                onClick = { navController.navigate(Screen.One.route) },
                icon = { Icon(imageVector = Icons.Default.Add, contentDescription = "Add Recipe", tint = MaterialTheme.colorScheme.onPrimary) }
            )
            NavigationBarItem(
                selected = (selectedScreen == Screen.Two),
                onClick = { navController.navigate(Screen.Two.route) },
                icon = { Icon(imageVector = Icons.Default.List, contentDescription = "Recipe List", tint = MaterialTheme.colorScheme.onPrimary) }
            )
            NavigationBarItem(
                selected = (selectedScreen == Screen.Three),
                onClick = { navController.navigate(Screen.Three.route) },
                icon = { Icon(imageVector = Icons.Default.Search, contentDescription = "Search Recipes", tint = MaterialTheme.colorScheme.onPrimary) }
            )
            NavigationBarItem(
                selected = (selectedScreen == Screen.Four),
                onClick = { navController.navigate(Screen.Four.route) },
                icon = { Icon(imageVector = Icons.Default.Person, contentDescription = "User Profile", tint = MaterialTheme.colorScheme.onPrimary) }
            )
        }
    }
}

@Composable
fun startScreen(mainViewModel: MainViewModel, navigateToScreenOne: () -> Unit) {
    var selectedDiet by remember { mutableStateOf<String?>(null) }

    Column(
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally,
        modifier = Modifier
            .fillMaxSize()
            .verticalScroll(rememberScrollState())
            .padding(vertical = 10.dp, horizontal = 16.dp)
    ) {
        Box {
            Text(
                text = "Welcome to WasteNot Wizard!",
                style = Typography.headlineLarge,
                modifier = Modifier
                    .padding(bottom = 20.dp)
                    .padding(horizontal = 16.dp),
                textAlign = TextAlign.Center
            )
        }
        Image(
            painter = painterResource(id = R.drawable.logo),
            contentDescription = "Logo",
            modifier = Modifier
                .size(200.dp)
        )
        Box {
            Text(
                text = "What is your preferred diet?",
                style = Typography.headlineSmall,
                modifier = Modifier
                    .padding(bottom = 20.dp)
                    .padding(horizontal = 16.dp)
                    .padding(top = 20.dp),
                textAlign = TextAlign.Center
            )
        }
        Column(
            verticalArrangement = Arrangement.spacedBy(8.dp),
            horizontalAlignment = Alignment.CenterHorizontally,
            modifier = Modifier
                .fillMaxWidth()
                .padding(horizontal = 16.dp)
        ) {
            DietButton("Meat", selectedDiet == "Meat") {
                selectedDiet = "Meat"
                mainViewModel.updateUserProfileDiet("Meat")
            }
            DietButton("Vegetarian", selectedDiet == "Vegetarian") {
                selectedDiet = "Vegetarian"
                mainViewModel.updateUserProfileDiet("Vegetarian")
            }
            DietButton("Vegan", selectedDiet == "Vegan") {
                selectedDiet = "Vegan"
                mainViewModel.updateUserProfileDiet("Vegan")
            }
        }
        Button(
            onClick = {
                selectedDiet?.let { mainViewModel.updateUserProfileDiet(it) }
                navigateToScreenOne()
            },
            modifier = Modifier
                .padding(top = 20.dp)
                .shadow(
                    elevation = 4.dp,
                    shape = CircleShape,
                    clip = true,
                )
        ) {
            Text(text = "Start", fontSize = 20.sp)
        }
    }
}

@Composable
fun mainScreen(mainViewModel: MainViewModel){

    var name by rememberSaveable(stateSaver = TextFieldValue.Saver) { mutableStateOf(TextFieldValue("")) }

    var selectedIngredients by remember { mutableStateOf("") }
    val ingredients = listOf(
        "Chicken",
        "Salmon",
        "Shrimp",
        "Tofu",
        "Carrot",
        "Potato",
        "Bell Pepper",
        "Garlic",
        "Onion",
        "Broccoli",
        "Asparagus",
        "Spaghetti",
        "Quinoa",
        "Olive Oil",
        "Soy Sauce",
        "Parmesan Cheese",
        "Lemon"
    )

    var selectedDiet by remember { mutableStateOf("") }
    val diet = listOf("meat", "vegetarian", "vegan")

    var selectedTime by remember { mutableStateOf("") }
    val time = listOf("0-15 minutes", "15-30 minutes", "30-60 minutes", "over 60 minutes")

    var notes by rememberSaveable(stateSaver = TextFieldValue.Saver) { mutableStateOf(TextFieldValue("")) }

    val state = mainViewModel.mainViewState.collectAsState()

    Column(
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally,
        modifier = Modifier
            .fillMaxSize()
            .verticalScroll(rememberScrollState())
            .background(MaterialTheme.colorScheme.background)
            .padding(vertical = 10.dp, horizontal = 16.dp)
    ) {
        Box {
            Text(
                text = "Add Recipe:",
                style = Typography.headlineLarge,
                modifier = Modifier
                    .padding(bottom = 20.dp)
                    .padding(top = 20.dp),
                textAlign = TextAlign.Center
            )
        }
        TextField(
            modifier = Modifier
                .fillMaxWidth()
                .padding(top = 20.dp)
                .padding(horizontal = 16.dp)
                .shadow(
                    elevation = 4.dp,
                    clip = true
                ),
            colors = TextFieldDefaults.textFieldColors(
                backgroundColor = MaterialTheme.colorScheme.tertiary
            ),
            value = name,
            onValueChange = { newText ->
                name = newText
            },
            label = {
                Text(text = "Recipe Name")
            }
        )
        DropdownTextFieldWithCheckbox<String>(
            modifier = Modifier
                .fillMaxWidth()
                .padding(top = 20.dp)
                .padding(horizontal = 16.dp),
            items = ingredients,
            label = "Ingredients",
            onItemsSelected = { selectedItems ->
                selectedIngredients = selectedItems.joinToString(", ")
            }
        )
        DropdownTextField(
            modifier = Modifier
                .fillMaxWidth()
                .padding(top = 20.dp)
                .padding(horizontal = 16.dp)
                .shadow(
                    elevation = 4.dp,
                    clip = true
                ),
            items = diet,
            label = "Diet Type",
            onItemSelected = { diet ->
                selectedDiet = diet
            }
        )
        DropdownTextField(
            modifier = Modifier
                .fillMaxWidth()
                .padding(top = 20.dp)
                .padding(horizontal = 16.dp)
                .shadow(
                    elevation = 4.dp,
                    clip = true
                ),
            items = time,
            label = "Cooking Time",
            onItemSelected = { time ->
                selectedTime = time
            }
        )
        TextField(
            modifier = Modifier
                .fillMaxWidth()
                .padding(top = 20.dp)
                .padding(horizontal = 16.dp)
                .shadow(
                    elevation = 4.dp,
                    clip = true
                ),
            colors = TextFieldDefaults.textFieldColors(
                backgroundColor = MaterialTheme.colorScheme.tertiary
            ),
            value = notes,
            onValueChange = { newText ->
                notes = newText
            },
            label = {
                Text(text = "Notes")
            }
        )
        Button(
            onClick = {
                mainViewModel.saveButton(
                    name.text,
                    selectedIngredients,
                    selectedDiet,
                    selectedTime,
                    notes.text
                )
            },
            modifier = Modifier
                .padding(top = 20.dp)
                .shadow(
                    elevation = 4.dp,
                    shape = CircleShape,
                    clip = true,
                )
        ) {
            Text(text = "Add Recipe", fontSize = 20.sp)
        }

        if (state.value.openDialog) {
            AlertDialog(
                onDismissRequest = {
                    mainViewModel.dismissDialog()
                },
                title = {
                    Box(
                        modifier = Modifier
                            .fillMaxWidth(),
                        contentAlignment = Alignment.Center
                    ) {
                        Text(
                            text = "Recipe successfully added to the cookbook!",
                            fontWeight = FontWeight.Bold,
                            modifier = Modifier
                                .padding(bottom = 20.dp)
                                .padding(top = 20.dp),
                            textAlign = TextAlign.Center
                        )
                    }
                },
                confirmButton = {
                    Box(
                        modifier = Modifier
                            .fillMaxWidth(),
                        contentAlignment = Alignment.Center
                    ) {
                        Button(
                            modifier = Modifier
                                .padding(bottom = 20.dp)
                                .shadow(
                                    elevation = 4.dp,
                                    shape = CircleShape,
                                    clip = true,
                                ),
                            onClick = {
                                mainViewModel.dismissDialog()
                            }
                        ) {
                            Text(text = "Okay", fontSize = 20.sp)
                        }
                    }
                }
            )
        }
    }
}

@Composable
fun recipeScreen(mainViewModel: MainViewModel) {
    var selectedDiet by remember { mutableStateOf<String?>("All Diets") }
    val dietList = listOf("meat", "vegetarian", "vegan")

    var selectedCookingTime by remember { mutableStateOf<String?>("All Times") }
    val timeList = listOf("0-15 minutes", "15-30 minutes", "30-60 minutes", "over 60 minutes")

    val allDiets = listOf("All Diets") + dietList
    val allCookingTimes = listOf("All Times") + timeList

    val state = mainViewModel.mainViewState.collectAsState()
    val filteredRecipes = state.value.recipes
        .filter { recipe ->
            (selectedDiet == null || selectedDiet == recipe.diet || selectedDiet == "All Diets") &&
                    (selectedCookingTime == null || selectedCookingTime == recipe.time || selectedCookingTime == "All Times")
        }

    LazyColumn(
        verticalArrangement = Arrangement.Top,
        horizontalAlignment = Alignment.CenterHorizontally,
        modifier = Modifier
            .fillMaxSize()
            .padding(horizontal = 16.dp)
    ) {
        item {
            Text(
                text = "My Recipes",
                style = Typography.headlineLarge,
                modifier = Modifier
                    .padding(bottom = 20.dp)
                    .padding(top = 40.dp)
            )
            Row(
                modifier = Modifier
                    .fillMaxWidth()
            ) {
                DietFilterDropdown(
                    modifier = Modifier.weight(1f),
                    selectedDiet = selectedDiet,
                    allDiets = allDiets,
                    onFilterSelected = { diet ->
                        selectedDiet = diet
                    }
                )
                CookingTimeFilterDropdown(
                    modifier = Modifier.weight(1f),
                    selectedCookingTime = selectedCookingTime,
                    allCookingTimes = allCookingTimes,
                    onFilterSelected = { time ->
                        selectedCookingTime = time
                    }
                )
            }
            Spacer(
                modifier = Modifier.height(20.dp)
            )
        }

        items(filteredRecipes) { recipe ->
            Box(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(vertical = 10.dp, horizontal = 16.dp)
                    .clip(MaterialTheme.shapes.medium)
                    .background(color = MaterialTheme.colorScheme.surfaceVariant)
            ) {
                Column(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(16.dp)
                ) {
                    Row(
                        modifier = Modifier
                            .fillMaxWidth(),
                        horizontalArrangement = Arrangement.SpaceBetween
                    ) {
                        Text(text = recipe.name, style = Typography.headlineSmall, modifier = Modifier.weight(1f))
                        Row(
                            modifier = Modifier
                                .wrapContentSize(align = Alignment.Center)
                        ) {
                            IconButton(onClick = { mainViewModel.editRecipe(recipe) }) {
                                Icon(Icons.Default.Edit, contentDescription = "Edit")
                            }
                            IconButton(onClick = { mainViewModel.clickDelete(recipe) }) {
                                Icon(Icons.Default.Delete, contentDescription = "Delete")
                            }
                        }
                    }
                    Column(
                        modifier = Modifier
                            .fillMaxWidth()
                            .padding(top = 8.dp)
                    ) {
                        Text(
                            text = buildAnnotatedString {
                                withStyle(style = SpanStyle(fontWeight = FontWeight.Bold)) {
                                    append("Ingredients: ")
                                }
                                append(recipe.ingredients)
                            }
                        )
                        Spacer(modifier = Modifier.height(4.dp))
                        Text(
                            text = buildAnnotatedString {
                                withStyle(style = SpanStyle(fontWeight = FontWeight.Bold)) {
                                    append("Diet Type: ")
                                }
                                append(recipe.diet)
                            }
                        )
                        Spacer(modifier = Modifier.height(4.dp))
                        Text(
                            text = buildAnnotatedString {
                                withStyle(style = SpanStyle(fontWeight = FontWeight.Bold)) {
                                    append("Cooking Time: ")
                                }
                                append(recipe.time)
                            }
                        )
                        Spacer(modifier = Modifier.height(4.dp))
                        Text(
                            text = buildAnnotatedString {
                                withStyle(style = SpanStyle(fontWeight = FontWeight.Bold)) {
                                    append("Notes: ")
                                }
                                append(recipe.notes)
                            }
                        )
                    }
                }
            }
        }
    }
    Column {
        editRecipeModal(mainViewModel)
    }
}

@Composable
fun editRecipeModal(mainViewModel: MainViewModel) {
    val state = mainViewModel.mainViewState.collectAsState()

    if (state.value.openDialog) {
        val recipeBeingEdited = state.value.editRecipe ?: return

        var name by rememberSaveable(stateSaver = TextFieldValue.Saver) {
            mutableStateOf(TextFieldValue(recipeBeingEdited.name))
        }

        var selectedIngredients by remember {
            mutableStateOf(recipeBeingEdited.ingredients)
        }
        val ingredients = listOf(
            "Chicken",
            "Salmon",
            "Shrimp",
            "Tofu",
            "Carrot",
            "Potato",
            "Bell Pepper",
            "Garlic",
            "Onion",
            "Broccoli",
            "Asparagus",
            "Spaghetti",
            "Quinoa",
            "Olive Oil",
            "Soy Sauce",
            "Parmesan Cheese",
            "Lemon"
        )

        var selectedDiet by remember {
            mutableStateOf(recipeBeingEdited.diet)
        }
        val diet = listOf("meat", "vegetarian", "vegan")

        var selectedTime by remember {
            mutableStateOf(recipeBeingEdited.time)
        }
        val time = listOf("0-15 minutes", "15-30 minutes", "30-60 minutes", "over 60 minutes")

        var notes by rememberSaveable(stateSaver = TextFieldValue.Saver) {
            mutableStateOf(TextFieldValue(recipeBeingEdited.notes))
        }

        AlertDialog(
            onDismissRequest = {
                mainViewModel.dismissDialog()
            },
            title = {
                Box(
                    modifier = Modifier
                        .fillMaxWidth(),
                    contentAlignment = Alignment.TopCenter,
                ) {
                    Text(
                        text = "Edit Recipe:",
                        style = Typography.headlineSmall,
                        modifier = Modifier
                            .padding(top = 20.dp)
                    )
                }
            },
            text = {
                Column(
                    modifier = Modifier
                        .padding(horizontal = 16.dp)
                ) {
                    TextField(
                        modifier = Modifier
                            .fillMaxWidth()
                            .padding(top = 20.dp)
                            .padding(horizontal = 16.dp),
                        colors = TextFieldDefaults.textFieldColors(
                            backgroundColor = MaterialTheme.colorScheme.tertiary
                        ),
                        value = name,
                        onValueChange = { newText ->
                            name = newText
                        },
                        label = {
                            Text(text = "Recipe Name")
                        }
                    )
                    DropdownTextFieldWithCheckbox<String>(
                        modifier = Modifier
                            .fillMaxWidth()
                            .padding(top = 20.dp)
                            .padding(horizontal = 16.dp),
                        items = ingredients,
                        label = "Ingredients",
                        onItemsSelected = { selectedItems ->
                            selectedIngredients = selectedItems.joinToString(", ")
                        }
                    )
                    DropdownTextField(
                        modifier = Modifier
                            .fillMaxWidth()
                            .padding(top = 20.dp)
                            .padding(horizontal = 16.dp),
                        items = diet,
                        label = "Diet Type",
                        onItemSelected = { diet ->
                            selectedDiet = diet
                        },
                    )
                    DropdownTextField(
                        modifier = Modifier
                            .fillMaxWidth()
                            .padding(top = 20.dp)
                            .padding(horizontal = 16.dp),
                        items = time,
                        label = "Cooking Time",
                        onItemSelected = { time ->
                            selectedTime = time
                        }
                    )
                    TextField(
                        modifier = Modifier
                            .fillMaxWidth()
                            .padding(top = 20.dp)
                            .padding(horizontal = 16.dp),
                        colors = TextFieldDefaults.textFieldColors(
                            backgroundColor = MaterialTheme.colorScheme.tertiary
                        ),
                        value = notes,
                        onValueChange = { newText ->
                            notes = newText
                        },
                        label = {
                            Text(text = "Notes")
                        }
                    )
                }
            },
            confirmButton = {
                Box(
                    modifier = Modifier
                        .fillMaxWidth(),
                    contentAlignment = Alignment.Center
                ) {
                    Button(
                        modifier = Modifier
                            .padding(top = 20.dp)
                            .shadow(
                                elevation = 4.dp,
                                shape = CircleShape,
                                clip = true,
                            ),
                        onClick = {
                            mainViewModel.saveEditedRecipe(
                                name.text,
                                selectedIngredients,
                                selectedDiet,
                                selectedTime,
                                notes.text,
                                recipeBeingEdited.id
                            )
                        }
                    ) {
                        Text(text = "Save", fontSize = 20.sp)
                    }
                }
            }
        )
    }
}

@Composable
fun searchScreen(mainViewModel: MainViewModel) {
    val searchedRecipes by mainViewModel.searchedRecipes.collectAsState()

    var selectedIngredients by remember { mutableStateOf(emptyList<String>()) }
    val ingredients = listOf(
        "Chicken",
        "Salmon",
        "Shrimp",
        "Tofu",
        "Carrot",
        "Potato",
        "Bell Pepper",
        "Garlic",
        "Onion",
        "Broccoli",
        "Asparagus",
        "Spaghetti",
        "Quinoa",
        "Olive Oil",
        "Soy Sauce",
        "Parmesan Cheese",
        "Lemon"
    )

    Column(
        verticalArrangement = Arrangement.Top,
        horizontalAlignment = Alignment.CenterHorizontally,
        modifier = Modifier
            .fillMaxSize()
            .verticalScroll(rememberScrollState())
            .padding(horizontal = 16.dp)
    ) {
        Box {
            Text(
                text = "Search Recipes:",
                style = Typography.headlineLarge,
                modifier = Modifier
                    .padding(bottom = 20.dp)
                    .padding(top = 40.dp),
                textAlign = TextAlign.Center
            )
        }
        DropdownTextFieldWithCheckbox<String>(
            modifier = Modifier
                .fillMaxWidth()
                .padding(top = 20.dp)
                .padding(horizontal = 16.dp),
            items = ingredients,
            label = "Ingredients",
            onItemsSelected = { selectedItems ->
                selectedIngredients = selectedItems
            }
        )
        Button(
            onClick = {
                mainViewModel.updateSelectedIngredients(selectedIngredients)
                mainViewModel.searchRecipesWithIngredients()
            },
            modifier = Modifier
                .padding(top = 20.dp)
                .shadow(
                    elevation = 4.dp,
                    shape = CircleShape,
                    clip = true,
                )
        ) {
            Text(text = "Search Recipes", fontSize = 20.sp)
        }
        Spacer(
            modifier = Modifier.height(20.dp)
        )

        LaunchedEffect(searchedRecipes) {
            Log.d("SEARCH_SCREEN", "Searched Recipes: $searchedRecipes")
        }

        searchedRecipes.forEach { recipe ->
            Box(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(vertical = 10.dp, horizontal = 16.dp)
                    .clip(MaterialTheme.shapes.medium)
                    .background(color = MaterialTheme.colorScheme.surfaceVariant)
            ) {
                Row(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(16.dp),
                ) {
                    Column(modifier = Modifier.weight(1f)) {
                        Text(text = recipe.name, style = Typography.headlineSmall)
                        Text(
                            text = buildAnnotatedString {
                                withStyle(style = SpanStyle(fontWeight = FontWeight.Bold)) {
                                    append("Ingredients: ")
                                }
                                append(recipe.ingredients)
                            },
                            modifier = Modifier.padding(top = 8.dp)
                        )
                        Spacer(modifier = Modifier.height(4.dp))
                        Text(
                            text = buildAnnotatedString {
                                withStyle(style = SpanStyle(fontWeight = FontWeight.Bold)) {
                                    append("Diet Type: ")
                                }
                                append(recipe.diet)
                            }
                        )
                        Spacer(modifier = Modifier.height(4.dp))
                        Text(
                            text = buildAnnotatedString {
                                withStyle(style = SpanStyle(fontWeight = FontWeight.Bold)) {
                                    append("Cooking Time: ")
                                }
                                append(recipe.time)
                            }
                        )
                        Spacer(modifier = Modifier.height(4.dp))
                        Text(
                            text = buildAnnotatedString {
                                withStyle(style = SpanStyle(fontWeight = FontWeight.Bold)) {
                                    append("Notes: ")
                                }
                                append(recipe.notes)
                            }
                        )
                    }
                }
            }
        }
    }
}

@Composable
fun userScreen(mainViewModel: MainViewModel) {
    val totalRecipeCount by mainViewModel.totalRecipeCount.observeAsState(initial = 0)

    var showDialog by remember { mutableStateOf(false) }

    Column(
        verticalArrangement = Arrangement.Top,
        horizontalAlignment = Alignment.CenterHorizontally,
        modifier = Modifier
            .fillMaxSize()
            .padding(horizontal = 16.dp)
    ) {
        Text(
            text = "Wizard #1",
            style = Typography.headlineLarge,
            modifier = Modifier
                .padding(bottom = 20.dp)
                .padding(top = 40.dp),
            textAlign = TextAlign.Center
        )
        Image(
            painter = painterResource(id = R.drawable.profile_picture),
            contentDescription = "Profile Picture",
            modifier = Modifier
                .size(160.dp)
        )
        Text(
            text = "Diet: ${mainViewModel.selectedDiet.value ?: "Not specified"}",
            fontWeight = FontWeight.Bold,
            modifier = Modifier.padding(top = 20.dp)
        )
        Button(
            onClick = { showDialog = true },
            modifier = Modifier
                .padding(top = 16.dp)
                .shadow(
                    elevation = 4.dp,
                    shape = CircleShape,
                    clip = true,
                )
        ) {
            Text(text = "Update diet", fontSize = 20.sp)
        }

        if (showDialog) {
            AlertDialog(
                onDismissRequest = {
                    showDialog = false
                },
                title = {
                    Box(
                        modifier = Modifier
                            .fillMaxWidth(),
                        contentAlignment = Alignment.TopCenter,
                    ) {
                        Text(
                            text = "Update Diet:",
                            style = Typography.headlineSmall,
                            modifier = Modifier
                                .padding(bottom = 20.dp)
                                .padding(top = 20.dp)
                        )
                    }
                },
                text = {
                    Column(
                        verticalArrangement = Arrangement.spacedBy(8.dp),
                        horizontalAlignment = Alignment.CenterHorizontally,
                        modifier = Modifier
                            .fillMaxWidth()
                    ) {
                        DietButton("Meat", mainViewModel.selectedDiet.value == "Meat") {
                            mainViewModel.updateUserProfileDiet("Meat")
                        }
                        DietButton("Vegetarian", mainViewModel.selectedDiet.value == "Vegetarian") {
                            mainViewModel.updateUserProfileDiet("Vegetarian")
                        }
                        DietButton("Vegan", mainViewModel.selectedDiet.value == "Vegan") {
                            mainViewModel.updateUserProfileDiet("Vegan")
                        }
                    }
                },
                confirmButton = {
                    Box(
                        modifier = Modifier
                            .fillMaxWidth(),
                        contentAlignment = Alignment.Center
                    ) {
                        Button(
                            onClick = {
                                mainViewModel.selectedDiet.let {
                                    mainViewModel.updateUserProfileDiet(it.value ?: "")
                                }
                                showDialog = false
                            },
                            modifier = Modifier
                                .padding(top = 20.dp, bottom = 20.dp)
                                .shadow(
                                    elevation = 4.dp,
                                    shape = CircleShape,
                                    clip = true,
                                ),
                        ) {
                            Text(text = "Save", fontSize = 20.sp)
                        }
                    }
                }
            )
        }
        Spacer(
            modifier = Modifier.height(20.dp)
        )
        Text(
            text = "All my saved recipes: $totalRecipeCount",
            fontWeight = FontWeight.Bold
        )
        Spacer(
            modifier = Modifier.height(50.dp)
        )
        Text(
            text = "My Impact",
            style = MaterialTheme.typography.headlineSmall
        )
        Spacer(
            modifier = Modifier.height(20.dp)
        )
        Text(
            text = buildAnnotatedString {
                withStyle(style = SpanStyle(fontWeight = FontWeight.Bold)) {
                    append("Saved Food: ")
                }
            }
        )
        Text(
            text = "Feature not implemented yet",
        )
        Spacer(
            modifier = Modifier.height(20.dp)
        )
        Text(
            text = buildAnnotatedString {
                withStyle(style = SpanStyle(fontWeight = FontWeight.Bold)) {
                    append("Saved Costs: ")
                }
            }
        )
        Text(
            text = "Feature not implemented yet",
        )
    }
}

@Composable
fun <T> DropdownTextField(
    modifier: Modifier = Modifier,
    items: List<T>,
    label: String,
    onItemSelected: (T) -> Unit
) {
    var expanded by remember { mutableStateOf(false) }
    var selectedItem by remember { mutableStateOf<T?>(null) }

    Box(modifier = modifier) {
        TextField(
            modifier = Modifier
                .fillMaxWidth()
                .clickable(
                    interactionSource = remember { MutableInteractionSource() },
                    indication = null
                ) {
                    expanded = true
                },
            value = selectedItem?.toString() ?: "",
            onValueChange = { },
            readOnly = true,
            colors = TextFieldDefaults.textFieldColors(
                backgroundColor = MaterialTheme.colorScheme.tertiary
            ),
            label = {
                Text(
                    text = label,
                    modifier = Modifier.clickable(
                        interactionSource = remember { MutableInteractionSource() },
                        indication = null
                    ) {
                        expanded = true
                    }
                )
            },
            trailingIcon = {
                Icon(
                    imageVector = Icons.Default.ArrowDropDown,
                    contentDescription = null,
                    modifier = Modifier.clickable {
                        expanded = true
                    }
                )
            }
        )
        DropdownMenu(
            expanded = expanded,
            onDismissRequest = { expanded = false },
            modifier = Modifier
                .padding(horizontal = 16.dp)
        ) {
            items.forEach { item ->
                DropdownMenuItem(
                    onClick = {
                        onItemSelected(item)
                        selectedItem = item
                        expanded = false
                    }
                ) {
                    Text(text = item.toString())
                }
            }
        }
    }
}


@Composable
fun <T> DropdownTextFieldWithCheckbox(
    modifier: Modifier = Modifier,
    items: List<String>,
    label: String,
    onItemsSelected: (List<String>) -> Unit
) {
    var expanded by remember { mutableStateOf(false) }
    var selectedItems by remember { mutableStateOf(emptyList<String>()) }

    Box(modifier = Modifier) {
        TextField(
            modifier = Modifier
                .fillMaxWidth()
                .padding(horizontal = 16.dp)
                .padding(top = 20.dp)
                .shadow(
                    elevation = 4.dp,
                    clip = true
                )
                .clickable(
                    interactionSource = remember { MutableInteractionSource() },
                    indication = null
                ) {
                    expanded = true
                },
            value = selectedItems.joinToString(", "),
            onValueChange = { },
            readOnly = true,
            colors = TextFieldDefaults.textFieldColors(
                backgroundColor = MaterialTheme.colorScheme.tertiary
            ),
            label = {
                Text(
                    text = label,
                    modifier = Modifier.clickable(
                        interactionSource = remember { MutableInteractionSource() },
                        indication = null
                    ) {
                        expanded = true
                    }
                )
            },
            trailingIcon = {
                Icon(
                    imageVector = Icons.Default.ArrowDropDown,
                    contentDescription = null,
                    modifier = Modifier.clickable {
                        expanded = true
                    }
                )
            }
        )
        DropdownMenu(
            expanded = expanded,
            onDismissRequest = { expanded = false },
            modifier = Modifier
                .padding(horizontal = 16.dp)
        ) {
            items.forEach { item ->
                val isChecked = selectedItems.contains(item)
                DropdownMenuItem(
                    onClick = {
                        if (isChecked) {
                            selectedItems = selectedItems.filter { it != item }
                        } else {
                            selectedItems = selectedItems + item
                        }
                        onItemsSelected(selectedItems)
                    }
                ) {
                    Checkbox(
                        checked = isChecked,
                        onCheckedChange = null,
                        modifier = Modifier.padding(end = 16.dp)
                    )
                    Text(text = item)
                }
            }
        }
    }
}

@Composable
fun DietFilterDropdown(
    modifier: Modifier = Modifier,
    selectedDiet: String?,
    allDiets: List<String>,
    onFilterSelected: (diet: String?) -> Unit
) {
    var expanded by remember { mutableStateOf(false) }

    Box(modifier = modifier) {
        TextField(
            modifier = Modifier
                .fillMaxWidth()
                .padding(horizontal = 16.dp)
                .shadow(
                    elevation = 4.dp,
                    clip = true
                )
                .clickable(
                    indication = null,
                    interactionSource = remember { MutableInteractionSource() },
                ) {
                    expanded = true
                },
            value = selectedDiet ?: "All Diets",
            onValueChange = { },
            readOnly = true,
            colors = TextFieldDefaults.textFieldColors(
                backgroundColor = MaterialTheme.colorScheme.tertiary
            ),
            label = {
                Text(
                    modifier = Modifier.clickable {
                        expanded = true
                    },
                    text = "Diet",
                    fontWeight = FontWeight.Bold,
                )
            },
            trailingIcon = {
                Icon(
                    imageVector = Icons.Default.ArrowDropDown,
                    contentDescription = null,
                    modifier = Modifier.clickable {
                        expanded = true
                    }
                )
            }
        )

        DropdownMenu(
            expanded = expanded,
            onDismissRequest = { expanded = false },
            modifier = Modifier
                .padding(horizontal = 16.dp)
        ) {
            allDiets.forEach { diet ->
                DropdownMenuItem(
                    onClick = {
                        onFilterSelected(diet)
                        expanded = false
                    }
                ) {
                    Text(text = diet)
                }
            }
        }
    }
}

@Composable
fun CookingTimeFilterDropdown(
    modifier: Modifier = Modifier,
    selectedCookingTime: String?,
    allCookingTimes: List<String>,
    onFilterSelected: (time: String?) -> Unit
) {
    var expanded by remember { mutableStateOf(false) }

    Box(modifier = modifier) {
        TextField(
            modifier = Modifier
                .fillMaxWidth()
                .padding(horizontal = 16.dp)
                .shadow(
                    elevation = 4.dp,
                    clip = true
                )
                .clickable(
                    indication = null,
                    interactionSource = remember { MutableInteractionSource() },
                ) {
                    expanded = true
                },
            value = selectedCookingTime ?: "All Times",
            onValueChange = { },
            readOnly = true,
            colors = TextFieldDefaults.textFieldColors(
                backgroundColor = MaterialTheme.colorScheme.tertiary
            ),
            label = {
                Text(
                    modifier = Modifier.clickable {
                        expanded = true
                    },
                    text = "Time",
                    fontWeight = FontWeight.Bold,
                )
            },
            trailingIcon = {
                Icon(
                    imageVector = Icons.Default.ArrowDropDown,
                    contentDescription = null,
                    modifier = Modifier.clickable {
                        expanded = true
                    }
                )
            }
        )

        DropdownMenu(
            expanded = expanded,
            onDismissRequest = { expanded = false },
            modifier = Modifier
                .padding(horizontal = 16.dp)
        ) {
            allCookingTimes.forEach { time ->
                DropdownMenuItem(
                    onClick = {
                        onFilterSelected(time)
                        expanded = false
                    }
                ) {
                    Text(text = time)
                }
            }
        }
    }
}

@Composable
fun DietButton(diet: String, isSelected: Boolean, onClick: () -> Unit) {
    Button(
        onClick = { onClick() },
        modifier = Modifier
            .shadow(
                elevation = 4.dp,
                shape = CircleShape,
                clip = true,
            ),
        colors = ButtonDefaults.buttonColors (
            containerColor = MaterialTheme.colorScheme.secondary,
            contentColor = MaterialTheme.colorScheme.onSecondary
        ),
        enabled = !isSelected
    ) {
        Text(text = diet, fontSize = 20.sp)
    }
}
