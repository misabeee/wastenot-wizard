package com.cc221021.wastenotwizard.ui.theme

import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.darkColorScheme
import androidx.compose.runtime.Composable

private val MeatColorScheme = darkColorScheme(
    primary = Purple35,
    onPrimary = White,
    secondary = Purple55,
    onSecondary = White,
    tertiary = Purple70,
    onTertiary = Black,
    background = Purple90,
    onBackground = Black,
    surface = Purple90,
    onSurface = Black,
    surfaceVariant = Purple80,
    onSurfaceVariant = Black
)

@Composable
fun WasteNotWizardTheme(content: @Composable () -> Unit) {
    MaterialTheme(
        colorScheme = MeatColorScheme,
        typography = Typography,
        shapes = Shapes,
        content = content
    )
}
