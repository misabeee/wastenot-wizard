package com.cc221021.wastenotwizard.ui.theme

import androidx.compose.ui.graphics.Color

val Purple35 = Color(101,44,135) // Buttons, Navigation bar
val Purple55 = Color(156,79,201) // Diet Buttons
val Purple70 = Color(189,138,219) // Text-fields
val Purple80 = Color(211,177,231) // Items
val Purple90 = Color(233,216,243) // Background, Alert Dialog

val White = Color.White // Text dark surfaces
val Black = Color.Black // Text light surfaces