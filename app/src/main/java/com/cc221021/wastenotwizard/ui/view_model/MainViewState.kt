package com.cc221021.wastenotwizard.ui.view_model

import com.cc221021.wastenotwizard.data.model.Recipe
import com.cc221021.wastenotwizard.ui.view.Screen

data class MainViewState(
    val selectedScreen: Screen = Screen.One,
    val recipes: List<Recipe> = emptyList(),
    val openDialog: Boolean = false,
    val editRecipe: Recipe? = null,
    val searchedRecipes: List<Recipe> = emptyList()
)