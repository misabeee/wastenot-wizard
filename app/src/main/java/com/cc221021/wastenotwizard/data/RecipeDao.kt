package com.cc221021.wastenotwizard.data

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import com.cc221021.wastenotwizard.data.model.Recipe
import kotlinx.coroutines.flow.Flow

@Dao
interface RecipeDao {
    @Insert
    suspend fun insertRecipe(recipe: Recipe)

    @Update
    suspend fun updateRecipe(recipe: Recipe)

    @Delete
    suspend fun deleteRecipe(recipe: Recipe)

    @Query("SELECT * FROM recipes")
    fun getRecipes(): Flow<List<Recipe>>

    @Query("SELECT * FROM recipes WHERE 1=1 AND ingredients LIKE '%' || :ingredient0 || '%' AND ingredients LIKE '%' || :ingredient1 || '%' AND ingredients LIKE '%' || :ingredient2 || '%'")
    suspend fun searchRecipeByIngredients(ingredient0: String, ingredient1: String, ingredient2: String): List<Recipe>

    @Query("SELECT COUNT(name) FROM recipes")
    suspend fun getTotalRecipeCount(): Int

    @Query("SELECT DISTINCT diet FROM recipes")
    suspend fun getDiets(): List<String>

    @Query("SELECT DISTINCT time FROM recipes")
    suspend fun getCookingTimes(): List<String>
}