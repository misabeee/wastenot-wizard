package com.cc221021.wastenotwizard.data

import androidx.room.Database
import androidx.room.RoomDatabase
import com.cc221021.wastenotwizard.data.model.Recipe

@Database(entities = [Recipe::class], version = 1)
abstract class RecipeDatabase : RoomDatabase() {
    abstract val dao: RecipeDao
}