package com.cc221021.wastenotwizard.data.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "recipes")
data class Recipe(
    val name: String,
    val ingredients: String,
    val diet: String,
    val time: String,
    val notes: String,
    @PrimaryKey(autoGenerate = true) val id: Int = 0
)